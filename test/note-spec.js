describe('note manager', function() {
  it('should add a note', function() {
    browser.get('http://localhost:9000/#/notes/');

    element(by.model('note.title')).sendKeys('new note');
    element(by.css('[class="btn btn-small btn-primary"]')).click();


    expect(browser.getCurrentUrl()).toEqual('http://localhost:9000/#/notes');
  });
});