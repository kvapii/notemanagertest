'use strict';

angular.module('NoteApp.notes', [])

AngularApp.controller('NotesCtrl', ['$scope','$state','Notes','Note', function ($scope,$state,Notes,Note) {

    $scope.notes = Notes.query();

    $scope.deleteNote = function(note){
        console.log(note);
        Note.delete(note);
        $state.go('notes');
    };
}]);

AngularApp.controller('NoteCtrl', ['$scope', '$stateParams','$state','Note',
    function ($scope, $stateParams, $state, Note) {

        $scope.note = Note.show({id: $stateParams.id});

        $scope.updateNote = function(){
            console.log($scope.note);
            Note.update($scope.note);
            $state.go('notes');
        };
    }
]);

AngularApp.controller('NewNoteCtrl', ['$scope', '$stateParams','$state','Notes',
    function ($scope, $stateParams, $state, Notes) {

        $scope.createNote = function(){
            console.log($scope.note);
            Notes.create($scope.note);
            $state.go('notes');
        };
    }
]);