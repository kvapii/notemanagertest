AngularApp = angular.module('NoteApp', [
  'ui.router',
    'NoteApp.noteServices',
    'NoteApp.notes',
]);

AngularApp.config([
  '$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/notes');

    $stateProvider.state('notes', {
      url: '/notes',
      templateUrl: '../views/notes.html',
      controller: 'NotesCtrl'
    })
        .state('notes.edit',{
          url: '/:id',
          views: {
            '@': {
              templateUrl: '../views/updateNote.html',
                  controller: 'NoteCtrl'
            }
          }
        })
        .state('notes.create',{
          url: '/:id',
          views: {
            '@': {
              templateUrl: '../views/createNote.html',
              controller: 'NewNoteCtrl'
            }
          }
        });
  }]);