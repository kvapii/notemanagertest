'use strict';
/**
 * Created by Pavel.Kvapil on 9.1.2015.
 */

var noteServices = angular.module('NoteApp.noteServices', ['ngResource']);

noteServices.factory('Notes',
    function($resource){
        return $resource('http://private-anon-70d0680fd-note10.apiary-mock.com/notes', {}, {
            query: {method:'GET', isArray:true},
            create: {method:'POST'}
        });
    });

noteServices.factory('Note',
    function($resource){
        return $resource('http://private-anon-70d0680fd-note10.apiary-mock.com/notes/:id', {}, {
            show: { method: 'GET'},
            update: { method: 'PUT', params: {id: '@id'}},
            delete: {method: 'DELETE', params: {id: '@id'}}
        })
    });